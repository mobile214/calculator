import 'dart:io';

class Calculator {
    String menu;
    int num1;
    int num2;

    Calculator(this.menu,this.num1, this.num2);

    void chooseMenu(String menu) {
      switch (menu) {
        case "1" :
          add();
          break;
        case "2" :
          subtract();
          break;
        case "3" :
           multiply();
           break;
        case "4" :
          divide();
          break;
        case "5" :
          break;
      }
    }  
    
    void add() {
      int res = num1+num2;
      print(res);
    }

    void subtract() {
      int res = num1-num2;
      print(res);
    }

    void multiply() {
      int res = num1*num2;
      print(res);
    }

    void divide() {
      int res = num1~/num2;
      print(res);
    }
}

