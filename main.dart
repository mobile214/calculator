import 'dart:io';
import 'calculator.dart';

void main(List<String> args) {
  showMenu();
  String menu = stdin.readLineSync()!;
  if (menu=="1" || menu == "2" || menu =="3" || menu == "4") {
    print("Enter the first value: ");
    int num1 = int.parse(stdin.readLineSync()!);
    print("Enter the second value: ");
    int num2 = int.parse(stdin.readLineSync()!);
    print("sum of the two numbers is : ");
    var calculate = Calculator(menu,num1,num2);
    calculate.chooseMenu(menu);
  }
 
}

void showMenu() {
  print("Menu");
  print("Select the choice you want to perform");
  print("1. ADD");
  print("2. SUBTRACT");
  print("3. MULTIPLY");
  print("4. DIVIDE");
  print("5. EXIT");
  print("Choose choice you want to enter : ");
}